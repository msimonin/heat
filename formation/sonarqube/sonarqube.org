#+TITLE: Static analysis and SonarQube web service
#+AUTHOR: Florent Pruvost, Service Expérimentation et Dévelopement, Inria Bordeaux Sud-Ouest
#+LANGUAGE:  en
#+OPTIONS: H:3 num:t toc:t \n:nil @:t ::t |:t _:nil ^:nil -:t f:t *:t <:t
#+OPTIONS: TeX:t LaTeX:t skip:nil d:nil pri:nil tags:not-in-toc html-style:nil
#+PROPERTY: header-args:sh :noweb no-export :session local :exports code :results none
#+HTML_HEAD:   <link rel="stylesheet" title="Standard" href="./css/worg.css" type="text/css" />
#+HTML_HEAD:   <link rel="stylesheet" type="text/css" href="./css/VisuGen.css" />
#+HTML_HEAD:   <link rel="stylesheet" type="text/css" href="./css/VisuRubriqueEncadre.css" />

This document presents an example of source code (C) analysis and
publication of results to the [[https://sonarqube.bordeaux.inria.fr/sonarqube][Inria SonarQube web server]].

Many concepts and tools that are used here are discussed in the [[https://sonarqube.bordeaux.inria.fr/pages/documentation.html][Inria
SonarQube documentation]]. Please refer to this documentation to get
more information.

The toy project used here is [[https://gitlab.inria.fr/sed-bso/heat][Heat]].

Summary of the hands-on session:
1. install prerequisites
2. download the source code from the public git repository
3. build the project: generate a library, executables and a report
   containing the GCC compiler warnings, and generate clang static
   analyzer reports
4. execute tests: necessary to produce the coverage (gcov) data
5. perform analysis: gcov, cppcheck, valgrind
6. create the sonar-project.properties file and call *sonar-scanner* to
   publish the results to SonarQube
7. analyze results with the SonarQube web interface

* Initial setup
** SonarQube prerequisites
   To be able to publish results to SonarQube a few steps are required:
   1. Log in to https://sonarqube.bordeaux.inria.fr/sonarqube with your
      Inria LDAP credential and create a [[https://sonarqube.bordeaux.inria.fr/pages/documentation.html#org693e559][personal token]] (save it in
      your home for example in ~$HOME/.sonarqubetoken~)
   3. Install the [[https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner][sonar-scanner program]], an example is given in the
      next section
   4. Import the [[https://sonarqube.bordeaux.inria.fr/pages/documentation.html#orga555ab3][certificate]]

   You should now be able to publish analysis reports on SonarQube.
** Install some development and analysis tools
   Install the Heat program dependencies (C compiler, CMake, MPI).
   #+begin_src sh
   sudo apt update
   sudo apt install -y build-essential cmake libopenmpi-dev
   #+end_src

   Install some analysis tools for C/C++ and Python programs
   #+begin_src sh
   sudo apt install -y clang gcovr lcov cppcheck vera++ valgrind python2.7 python-pip pylint
   pip install --upgrade pip
   sudo python -m pip install pytest pytest-cov coverage==3.7.1 scan-build
   #+end_src

   Install rats (not in Debian packages anymore, a pity)
   #+begin_src sh
   sudo apt install -y autoconf automake libexpat1-dev
   wget https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/rough-auditing-tool-for-security/rats-2.4.tgz
   tar -xzvf rats-2.4.tgz
   cd rats-2.4
   ./configure && make && sudo make install
   #+end_src

   Install the script to convert vera++ output into xml file
   compatible with SonarQube
   #+begin_src sh
   git clone https://github.com/SonarOpenCommunity/sonar-cxx.git
   chmod +x $PWD/sonar-cxx/cxx-sensors/src/tools/vera++Report2checkstyleReport.perl
   sudo ln -s $PWD/sonar-cxx/cxx-sensors/src/tools/vera++Report2checkstyleReport.perl /usr/local/bin/vera++Report2checkstyleReport.perl
   #+end_src

   Install the script to convert lcov coverage report into xml file
   compatible with SonarQube
   #+begin_src sh
   wget https://github.com/eriwen/lcov-to-cobertura-xml/archive/1.6.tar.gz
   tar xvf 1.6.tar.gz
   sudo ln -s $PWD/lcov-to-cobertura-xml-1.6/lcov_cobertura/lcov_cobertura.py /usr/local/bin/lcov_cobertura.py
   #+end_src

   Install sonar-scanner (if not already done)
   #+begin_src sh
   wget https://sonarsource.bintray.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-3.2.0.1227-linux.zip
   unzip sonar-scanner-cli-3.2.0.1227-linux.zip
   keytool -import -alias inria -storepass "changeit" -keystore $PWD/sonar-scanner-3.2.0.1227-linux/jre/lib/security/cacerts -file digicertca.crt
   sudo ln -s $PWD/sonar-scanner-3.2.0.1227-linux/bin/sonar-scanner /usr/local/bin/sonar-scanner
   #+end_src
* Download
  Download the *Heat* source code, this is a public git repository
  #+name: download
  #+begin_src sh
  # git command is required
  git clone https://gitlab.inria.fr/sed-bso/heat.git
  cd heat/
  #+end_src

* Build
  Full configuration and build process (with Clang-SA scan)
  #+name: install
  #+begin_src sh
  mkdir -p build
  cd build
  # make clean
  export CFLAGS="--coverage -fPIC -fdiagnostics-show-option -Wall -Wunused-parameter -Wundef -Wno-long-long -Wsign-compare -Wmissing-prototypes -Wstrict-prototypes -Wcomment -pedantic -g"
  export LDFLAGS="--coverage"
  cmake .. -DHEAT_USE_MPI=ON -DCMAKE_VERBOSE_MAKEFILE=ON -DCMAKE_C_FLAGS="$CFLAGS" -DCMAKE_EXE_LINKER_FLAGS="$LDFLAGS"
  scan-build -v -plist --intercept-first --analyze-headers -o analyzer_reports make 2>&1 | tee heat-build.log
  #+end_src
  Remarks:
  - "--coverage" is used for coverage analysis with *gcov*, see
    https://gcc.gnu.org/onlinedocs/gcc/Instrumentation-Options.html#Instrumentation-Options
  - "-fPIC" should be used if a shared library is built
  - "-fdiagnostics-show-option" is used to feed GCC warnings into
    SonarQube, see
    https://github.com/SonarOpenCommunity/sonar-cxx/wiki/Compilers
  - "-Wall -Wunused-parameter -Wundef -Wno-long-long -Wsign-compare
    -Wmissing-prototypes -Wstrict-prototypes -Wcomment -pedantic" are
    used to enable some GCC warnings (arbitrarily)
  - *scan-build* program is used to perform a clang static analyzer
    scan, see https://clang-analyzer.llvm.org/scan-build.html
  - we build with debug information "-g" to help *scan-build* (clang
    analyzer) to be more effective (avoid false positive), see
    https://clang-analyzer.llvm.org/scan-build.html#recommended_debug
  - scan-build results are stored in ~analyzer_reports~
* Test
  We perform unitary tests and this generates gcov coverage files
  thanks to the "--coverage" flags used just before, see resulting
  /*.gcda/ and /*.gcno/ files
  #+name: tests
  #+begin_src sh
  ctest -V
  #+end_src

  #+begin_src sh
  find . -regex '.*\.\(gcda\|gcno\)'
  #+end_src
* Perform static and dynamic analysis
  SonarQube performs a static analysis when we invoke *sonar-scanner*
  (see the next section "SonarQube scan"). The issues that are
  considered by the SonarQube analysis depends on the language and for
  C/C++ it relies on the [[https://github.com/SonarOpenCommunity/sonar-cxx][sonar-cxx plugin]]. In addition to its own
  analysis the plugin allows to import issues addressed by external
  analysis tools.

  In this section we run some of the external tools that are
  compatible to perform static (cppcheck, rats, vera++) and dynamic
  analysis (coverage, valgrind).

  Notice we already have performed the clang static analysis during
  the build step. There is no need to do somthing alse, the clang
  reports are directly compatible with SonarQube.

** coverage
   After the tests execution you can generate the coverage report. You
   can already generate an HTML file using *lcov* and *genhtml*.
   *lcov_cobertura.py* script is necessary to produce the xml report
   necessary to publish coverage on SonarQube (the .lcov report cannot
   be imported as it is).
   #+name: coverage
   #+begin_src sh
   cd ..
   lcov --directory . --capture --output-file heat.lcov
   genhtml -o coverage heat.lcov
   # see the result, for example: chromium-browser coverage/index.html
   lcov_cobertura.py heat.lcov --output heat-coverage.xml
   #+end_src
   See
   https://sonarqube.bordeaux.inria.fr/pages/documentation.html#org9e9e84d
   for more information.
** cppcheck
   See
   https://sonarqube.bordeaux.inria.fr/pages/documentation.html#org4575413.
   #+name: cppcheck
   #+begin_src sh
   export DEFINITIONS=""
   export CPPCHECK_INCLUDES="-I."
   export SOURCES_TO_EXCLUDE="-ibuild/CMakeFiles/"
   export SOURCES_TO_ANALYZE="."
   cppcheck -v -f --language=c --platform=unix64 --enable=all --xml --xml-version=2 --suppress=missingIncludeSystem ${DEFINITIONS} ${CPPCHECK_INCLUDES} ${SOURCES_TO_EXCLUDE} ${SOURCES_TO_ANALYZE} 2> heat-cppcheck.xml
   #+end_src
   Remarks:
   - "-f" to force maximum number of configurations (all macros values
     combination), see "--max-configs" option to limit the number of
     configurations because it can be very long
   - "--enable=all" enables all error types checking
   - "--xml" allows to generate the report for SonarQube
** rats
   See
   https://sonarqube.bordeaux.inria.fr/pages/documentation.html#org2f9daa7.
   #+name: rats
   #+begin_src sh
   export SOURCES_TO_ANALYZE="."
   rats -w 3 --xml ${SOURCES_TO_ANALYZE} > heat-rats.xml
   #+end_src
** vera++
   See
   https://sonarqube.bordeaux.inria.fr/pages/documentation.html#orgec582f5.
   #+name: vera
   #+begin_src sh
   export SOURCES_TO_ANALYZE="."
   bash -c 'find ${SOURCES_TO_ANALYZE} -regex ".*\.c\|.*\.h" | vera++ - -showrules -nodup |& vera++Report2checkstyleReport.perl > heat-vera.xml'
   #+end_src
** valgrind
   See
   https://sonarqube.bordeaux.inria.fr/pages/documentation.html#orge2639b3.
   [[http://valgrind.org/][Valgrind]] is used to detect memory management and threading bugs.
   #+name: valgrind
   #+begin_src sh
   export OPENMPI_DIR=/usr
   $OPENMPI_DIR/bin/mpirun "-np" "4" valgrind --xml=yes --xml-file=heat-valgrind.xml --memcheck:leak-check=full --show-reachable=yes --suppressions=$OPENMPI_DIR/share/openmpi/openmpi-valgrind.supp --suppressions=tools/heat-valgrind.supp "./build/heat_par" "10" "10" "200" "2" "2" "0"
   #+end_src
   The ~heat-valgrind.supp~ file has been created to suppress memcheck
   errors related with MPI that are not handled by the suppression
   file providen by MPI, /e.g./
   ~$OPENMPI_DIR/share/openmpi/openmpi-valgrind.supp~.
* SonarQube scan
  SonarQube performs a static analysis when we invoke
  *sonar-scanner*. The issues that are considered by the SonarQube
  analysis depends on the language and for C/C++ it relies on the
  [[https://github.com/SonarOpenCommunity/sonar-cxx][sonar-cxx plugin]].

  The *sonar-scanner* program is used to execute the SonarQube scan and
  put the results online /i.e./ upload to the [[https://sonarqube.bordeaux.inria.fr/sonarqube][server]]. The program takes
  some arguments such as the url of the server, credentials, the name
  we want to assign to our project analysis, paths to external
  analysis reports, etc.

  See [[https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner#AnalyzingwithSonarQubeScanner-Use][how to use it]].  Parameters can be given either with the -D
  argument of the program or in defining them in a configuration file
  ~sonar-project.properties~ which must be located in the current path
  where sonar-scanner is invoked.

  #+begin_example
  sonar-scanner -Dsonar.host.url=https://sonarqube.bordeaux.inria.fr/sonarqube -Dsonar.login=8c413fbfb15c59206ee73ec6d6bf45c4eac190e3 -Dsonar.projectName=Heat
  #+end_example

** config scanner
   For our use case we define all parameters in the configuration file
   #+name: scanconfig
   #+begin_src sh
   cat > sonar-project.properties << EOF
   sonar.host.url=https://sonarqube.bordeaux.inria.fr/sonarqube
   sonar.links.homepage=https://gitlab.inria.fr/sed-bso/heat
   sonar.links.scm=https://gitlab.inria.fr/sed-bso/heat.git
   sonar.projectKey=sedbso:heat:gitlab:master
   sonar.projectDescription=Solve the heat propagation equation
   sonar.projectVersion=1.0
   sonar.login=`cat ~/.sonarqubetoken`
   sonar.scm.disabled=false
   sonar.scm.provider=git
   sonar.sourceEncoding=UTF-8
   sonar.sources=.
   sonar.exclusions=build/CMakeFiles/**
   sonar.language=c
   sonar.c.errorRecoveryEnabled=true
   sonar.c.compiler.parser=GCC
   sonar.c.includeDirectories=$(echo | gcc -E -Wp,-v - 2>&1 | grep "^ " | tr '\n')
   sonar.c.compiler.charset=UTF-8
   sonar.c.compiler.regex=^(.*):(\\\d+):\\\d+: warning: (.*)\\\[(.*)\\\]$
   sonar.c.compiler.reportPath=build/heat-build.log
   sonar.c.clangsa.reportPath=build/analyzer_reports/*/*.plist
   sonar.c.coverage.reportPath=heat-coverage.xml
   sonar.c.cppcheck.reportPath=heat-cppcheck.xml
   sonar.c.rats.reportPath=heat-rats.xml
   sonar.c.vera.reportPath=heat-vera.xml
   sonar.c.valgrind.reportPath=heat-valgrind.xml
   EOF
   #+end_src
   Remarks:
   - for explanation about the main sonar-scanner parameters please
     read
     - https://sonarqube.bordeaux.inria.fr/pages/documentation.html#org8a50dfe
     - https://docs.sonarqube.org/display/SONAR/Analysis+Parameters
   - please follow the convention we described [[https://sonarqube.bordeaux.inria.fr/pages/documentation.html#orgda0f182][here]] about the
     *sonar.projectKey* (/e.g./ team:project:git:branch) parameter, and
     please do not provide the *sonar.projectName* (projectKey will be
     used as name), it helps to keep projects organized in SonarQube
   - the ~.sonarqubetoken~ file must contain your personal token in this
     example. You need to give your personal token here to be able to
     upload some data, see the first section "Initial setup"
   - the *sonar.c* parameters are related to the [[https://github.com/SonarOpenCommunity/sonar-cxx/wiki][sonar-cxx]] plugin and
     allows to complete the analysis with reports coming from other
     tools. In this example we import issues coming from
     - GCC warnings: compiler.reportPath
     - Clang-SA: clangsa.reportPath
     - GCOV: coverage.reportPath
     - Cppcheck: cppcheck.reportPath
     - Rats: rats.reportPath
     - Vera++: vera.reportPath
     - Valgrind: valgrind.reportPath

** sonar-scanner
   We invoke sonar-scanner
   #+name: scanner
   #+begin_src sh
   sonar-scanner -X >sonar.log 2>&1
   #+end_src

* Analyze results with the SonarQube web interface
  Official [[https://docs.sonarqube.org/display/SONARQUBE67/Documentation][SonarQube documentation]].
** Overview
   See https://docs.sonarqube.org/display/SONARQUBE67/Project+Page.
** Issues
   See https://docs.sonarqube.org/display/SONARQUBE67/Issues.
** Measures
   See
   https://docs.sonarqube.org/display/SONARQUBE67/Metric+Definitions.
** Code
   See https://docs.sonarqube.org/display/SONARQUBE67/Code+Viewer.
** Activity
   See
   https://docs.sonarqube.org/display/SONARQUBE67/Activity+and+History.
** Administration
   See
   https://docs.sonarqube.org/display/SONARQUBE67/Project+Administration+Guide.
*** Quality Profiles
    You can change the rules applied to your project, see
    https://docs.sonarqube.org/display/SONARQUBE67/Project+Settings#ProjectSettings-QualityGateandSettingQualityProfiles.
*** Permissions
    You can update permissions to make the project public or private
    (to hide or not the source code), add and remove users to be able
    to administrate the project and its issues. See also
    https://sonarqube.bordeaux.inria.fr/pages/faq.html#faq1.
** Email notifications
   You can register to email notifications that inform about new
   analysis results and give a link to the project new issues.

   Go to "My Account" (top right corner) -> "Notifications", in
   "Notifications per project", write the name of your project in the
   search bar, select it then check the boxes.

* Create a bash script to save the overall scan commands            :COMMENT:
  #+begin_src sh :eval no :tangle heat.sh :shebang "#!/bin/bash" :export none

  # Download and untar sources
  <<download>>

  # Configure, Make (with clang-sa analysis using scan-build)
  <<install>>

  # Execute unitary tests (autotest)
  <<tests>>

  # Collect coverage data
  <<coverage>>

  # Run cppcheck analysis
  <<cppcheck>>

  # Run rats analysis
  <<rats>>

  # Run vera++ analysis
  <<vera>>

  # Run valgrind analysis
  <<valgrind>>

  # Create the config for sonar-scanner
  <<scanconfig>>

  # Run the sonar-scanner analysis and submit to SonarQube server
  <<scanner>>

  #+end_src
